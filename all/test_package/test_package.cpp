#include "fx.h"

#include <iostream>

int main(void) {
    auto dir = FXDir{};
    dir.open(".");

    FXString filename;

    while (dir.next(filename)) {
        std::cout << filename.text() << std::endl;
    }

    return 0;
}
